ewoksutils |version|
====================

*ewoksutils* provides utilities for `ewoks <https://ewoks.readthedocs.io/>`_ developers.

*ewoksutils* has been developed by the `Software group <https://www.esrf.fr/Instrumentation/software>`_ of the `European Synchrotron <https://www.esrf.fr/>`_.

.. toctree::
    :hidden:

    events
    api
