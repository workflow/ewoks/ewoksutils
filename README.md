# ewoksutils

*ewoksutils* provides utilities for [ewoks](https://ewoks.readthedocs.io/) developers.

## Install

```bash
pip install ewoksutils[test]
```

## Test

```bash
pytest --pyargs ewoksutils.tests
```

## Documentation

https://ewoksutils.readthedocs.io/
