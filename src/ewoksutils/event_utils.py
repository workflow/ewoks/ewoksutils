FIELD_TYPES = {
    "host_name": "",
    "process_id": 0,
    "user_name": "",
    "job_id": "",
    "engine": "",
    "context": "",
    "workflow_id": "",
    "type": "",
    "time": "",
    "error": False,
    "error_message": "",
    "error_traceback": "",
    "node_id": "",
    "task_id": "",
    "progress": 0,
    "task_uri": "",
    "input_uris": list(),
    "output_uris": list(),
}
